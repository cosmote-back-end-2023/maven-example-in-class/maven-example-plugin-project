package com.learningactors.ote.plugin_example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "project-version", defaultPhase = LifecyclePhase.PACKAGE)
public class ProjectVersionFileExample extends AbstractMojo {

    @Parameter(defaultValue = "${project.version}", property = "projectVersion", required = true)
    private String projectVersion;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        {
            getLog().info("Executing Project Version Maven Plugin Example - Creating File for project version: " + projectVersion);
            String fileName = "project-version.txt";

            File touch = new File(fileName);

            try (FileWriter w = new FileWriter(touch)) {
                w.write("project.version: " + this.projectVersion);
            } catch (IOException e) {
                throw new MojoExecutionException("Error creating file " + touch, e);
            }
        }

    }
}
