# MAVEN PROJECT VERSION FILE PLUGIN EXAMPLE

This repo is an example of development of a simple maven plugin that runs default on the package phase of the project
that makes use of it.

The naming of the plugin follows the maven naming convention `project-version-file-maven-plugin`

###### INCLUSION - USAGE
In order to include the plugin in a maven project use the below snippet under `<plugins>`

```xml
    <plugin>
    <groupId>com.learningactors.ote</groupId>
    <artifactId>project-version-file-maven-plugin</artifactId>
    <version>1.0-SNAPSHOT</version>
    <executions>
        <execution>
            <id>make-version-file</id>
            <phase>package</phase>
            <goals>
                <goal>project-version</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

###### EXECUTION
Due to the above the plugin is executed:
* In the package phase (mvn package) and beyond
* By direct execution : `mvn com.learningactors.ote:project-version-file-maven-plugin:project-version`  
* By shortened name command: `mvn project-version-file:project-version`




